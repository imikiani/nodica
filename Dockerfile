FROM node:14-alpine
WORKDIR /app
COPY package.json .
COPY yarn.lock .
COPY index.js .
RUN yarn install
EXPOSE 3000
CMD ["node", "index.js"]
