'use strict';

const express = require('express');

const PORT = 3000
// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello Friends!');
});

app.listen(PORT);
console.log(`Listening on port ${PORT}`);
